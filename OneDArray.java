public class OneDArray
{
    public static void main(String[] args){


	//sequential sort
	//A - an array containing the list of numbers
	int[] a={4,17,23,11,22,7};
	//numItems - the number of numbers in the list
	int numItems=a.length;

	//sorting number
	for(int i=0;i<numItems-1;i++){
	    for(int j=i+1;j<numItems;j++){
		if(a[i]>a[j]){
		    int temp=a[i];
		    a[i]=a[j];
		    a[j]=temp;
		}
	    }
	}

	//displaying the sorted list
	for(int i=0;i<numItems;i++){
	    System.out.println(a[i]);
	}
    }


}
