public class While2{

    public static void main(String[] args){
	
	System.out.println("*******************************");
	System.out.println("WElcome to odd even calculator");
	System.out.println("*******************************");
	java.util.Scanner sc= new java.util.Scanner(System.in);
	System.out.println("\n\nDo you want to start the program??y for yes, n for no");
	String input=sc.next();
	int a=Integer.MIN_VALUE;
	while(input.equalsIgnoreCase("y")){
	    System.out.println("\n\n Enter a number");
	    a=sc.nextInt();
	    System.out.println( a%2==0?"Even number":"Odd Number");
	    System.out.println("\n\n Do you want to continue? type y for yes and n for no");
	    input=sc.next();
	}
		
	System.out.println("*******************************");
	System.out.println("Thank you for using  odd even calculator");
	System.out.println("*******************************");
	
    }
}
