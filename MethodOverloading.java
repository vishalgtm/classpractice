public class MethodOverloading{

    public int add(int a, int b){
	return a+b;
    }
    public float  add(float a, float b){
	return a+b;
    }

    public static void main(String[] args){
	MethodOverloading mo = new MethodOverloading();
	System.out.println(mo.add(2,3));
	System.out.println(mo.add(4,5f,3.4f));
    }

    println(int a){

    }
    println(String a){

    }
    println(double d){

    }

}
