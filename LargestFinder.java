interface Largest{
    
    String largestName(String a, String b);
    int largestNumber(int a, int b);

}


public class LargestFinder{

    public static void main(String[] args){
	Largest large= new Largest(){
		@Override
		public String largestName(String a, String b){
		    return a.length()>b.length()? a:b;
		}
		@Override
		public int largestNumber(int a, int b){
		    return a>b?a:b;
		}

	    };

	System.out.println(large.largestName("ramesh","hari"));
	System.out.println(large.largestNumber(45,55));

    }


}
