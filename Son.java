class Father{

    public String getAttitude(){
	return "simple";
    }

}

public class Son extends Father{
    @Override
    public String getAttitude(){
	return "angry";
    }

    public static void main(String[] args){
	Father s= new Father();
	System.out.println(s.getAttitude());

	Father si= new Son();
	System.out.println(si.getAttitude());

    }


}
