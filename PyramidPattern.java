public class PyramidPattern{
    public static void main(String[] args){
	int n=5;
	int inc_val=1;
	for(int i=1;i<=n;i++){
	    for(int j=n-1;j>=i;j--){
		System.out.print(" ");
	    } 
	    for(int k=1;k<=inc_val;k++){
		System.out.print("*");
		
	    }
	    inc_val+=2;
	    System.out.println();
	}
    }
}
