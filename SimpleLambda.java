interface Hello{

    void hello();

}

public class SimpleLambda{

    public static void main(String[] args){
	
	Hello h = new Hello(){
		@Override
		public void hello(){
		    System.out.println("hello from lambda epxression");
		}

	    };
	
    }


}
